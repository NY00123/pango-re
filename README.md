# Reconstructed x86 assembly sources for Pango (1980s DOS game)

This code tree covers reconstructed sources for the DOS game
from the 1980s, Pango. It is an unofficial adaptation of the
arcade game Pengo (note the change in the 2nd letter).
With the tools further suggested in this document, the original executable
could be byte-by-byte reproduced. Your mileage may vary as usual.

## Terms of use

The game itself is identified as (C) Copyright 1983 by Sheng-Chung LIU.
It is also a clone of an arcade game as previously mentioned.
Therefore, consider the reconstructed sources as available
for limited non-commercial educational purposes.

No guarantee of any kind is granted. Use at your own risk.

## Building PANGO.EXE from the sources

You should use The Microsoft MACRO Assembler (MASM) Version 1.10,
along with the IBM Personal Computer Link Version 1.10. While technically
optional, the Microsoft Program Maintenance Utility (NMAKE) Version 1.00.05
is further useful.

As an alternative, you may use version 5.10 of MASM instead of 1.10.

## Notes about the sources and the process of reconstructing them

MENU.ASM has strings using characters outside of ASCII's printable characters.
Since the sources are reconstructed, a decision was made to represent
these characters using numbers, so MENU.ASM itself would use
printable ASCII characters for increased compatibility.

While reversing the code base, it was assumed it had originally been
written in x86 assembly. In particular, there's no C library startup code
(or similar) and there's clearly more than one calling convention in use.
Just for a few examples, the BX register is repeatedly used to store
a location on the map (often of the Pango character), CF might state if a
round should end and CX often stores an on-screen offset in video memory.

Additionally, outside of using a call stack for function calls
and pushed register values, stack memory does not appear
to be used for local variables at all.

This game does not make use of custom interrupt handlers for purposes like
timing or sound playback. In addition to using the system clock, time to wait
is repeatedly measured using instructions, so the game's speed is CPU-dependent.
The latter was common for early DOS games of the 1980s.

Figuring out the meanings of functions and variables was taking its time,
as expected. The lack of startup code as described above made it easier
to identify the actual starting point, where the player is asked about
monitor and joystick use. Certain code pieces took more time to figure out,
but generally speaking, having enough definitions covered assisted with
figuring out more of these, gradually filling missing information over time.
In a few cases, hex-edits of the original EXE were done
in order to verify the meanings of certain contents.
Among other modifications, these included changes to locations of in-game
objects as shown in the instructions, edits to the first round (map) and further
edits of a few sound frequencies. Also covered were disabling a few functions
(at least ones for drawing) by using early returns from functions.

No debugger was used for reversing, up to and including the point of recreating
the EXE byte-by-byte for the first time. An exception to this came afterwards,
making use of a debugger-enabled DOSBox build in order to figure out
an explanation for the following bug.

The game may freeze upon reaching the score of 327680 or greater. The cause of
this is in a function used for printing the score. It is first divided by 10 to
get the quotient of 32768 or greater written to the AX register. This value is
then sign-extended to DX:AX. With AX >= 32768 as an unsigned, all bits of DX
are set. As a consequence, the result of a second unsigned division
of DX:AX by 10 overflows.

Link to a video demonstrating the bug during its last 30 seconds
(URL checked on May 2024): https://www.youtube.com/watch?v=UbmJwIQJx-s

To finish, SNDMUS.ASM implies the file covers sound and music playback
altogether. Technically speaking, that is correct, at least to an extent.
It has routines for turning the speaker on and off, repeatedly used from the
intro sequence. It also has clear music definitions.
However, regarding the sound played upon obtaining a bonus after completing
a round, it can be claimed that it's another case of a (short) music track.
With such a definition in mind, outside of the routines turning speaker on
and off, the code in SNDMUS.ASM really covers music playback only
(assuming the aforementioned definition in mind again).
In fact, that's something being realized just a bit before
the initial public release of the reconstructed sources.

-Yoav N.
